﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quan_Ly_Thong_Tin
{
    public class NguoiLaoDong
    {
        public NguoiLaoDong(string hoTen, int namSinh, double luongCoBan)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
        }
        public NguoiLaoDong() { }

        private string HoTen { get; set;}
        private int NamSinh { get; set;}
        private double LuongCoBan { get; set;}

        public virtual double TinhLuong()
        {
            return LuongCoBan;
        }

        public virtual void NhapThongTin(string hoTen, int namSinh, double luongCoBan)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
        }

        public virtual void XuatThongtin()
        {
            Console.WriteLine("Ho ten la: {0}, nam sinh: {1}, luong co ban: {2}.",HoTen,NamSinh,LuongCoBan);
        }


    }



    

}
