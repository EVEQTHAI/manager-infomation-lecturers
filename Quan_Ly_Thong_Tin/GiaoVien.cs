﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Quan_Ly_Thong_Tin
{
    public class GiaoVien : NguoiLaoDong
    {

        private double HeSoLuong { get; set; }

        public GiaoVien() { }

        public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong) : base(hoTen, namSinh, luongCoBan)
        {
            HeSoLuong = heSoLuong;
        }

        public void NhapThongTin(double heSoLuong)
        {
            HeSoLuong = HeSoLuong;
        }

        public override double TinhLuong()
        {
            return base.TinhLuong() * HeSoLuong * 1.25;   
        }

        public override void XuatThongtin()
        {
            base.XuatThongtin();
            Console.WriteLine("He so luong:{0}, Luong: {1}",HeSoLuong,this.TinhLuong());
        }
        
        public double XuLy()
        {
            return HeSoLuong + 0.6;
        }
    }
}
