﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quan_Ly_Thong_Tin
{
    public class Program
    {
        public static void Main(String[] args)
        {
            List<GiaoVien>soluongNV = new List<GiaoVien>();
            int n = getInt("Nhap so luong giao vien: ", "So luong nhan vien phai la chu so!!!", "So luong nhan vien lon hon 0!!!");

            for (int i = 1; i <= n; i++) 
            {
                Console.Write("Nhap ten Giao vien {0}: ", i);
                string hoTen = Console.ReadLine();
                //Console.Write("Nhap nam sinh: ");
                //int namSinh = int.Parse(Console.ReadLine());

                int namSinh = getAnInteger("Nhap vao nam sinh: ", "Nam sinh phai tu 1999...3000!!!", 1999, 3000);

                //Console.Write("Luong co ban: ");
                //double luongCoBan = double.Parse(Console.ReadLine());

                double luongCoBan = getDouble("Luong co ban: ", "Luong co ban phai la so!!!", "Luong co ban phai lon hon 0!!!");

                //Console.Write("Nhap he so luong: ");
                //double hsl = double.Parse(Console.ReadLine());

                double hsl = getDouble("Nhap he so luong: ", "He so luong phai la chu so!!!", "He so luong phai lon hon 0!!!");

                GiaoVien _gv = new GiaoVien(hoTen, namSinh, luongCoBan, hsl);

                soluongNV.Add(_gv);
            }

            var gv_LuongThapNhat = soluongNV.OrderBy(p=>p.TinhLuong()).FirstOrDefault();
            if(gv_LuongThapNhat != null)
            {
                Console.WriteLine("Nhan vien co luong thap nhat: ");
                gv_LuongThapNhat.XuatThongtin();
            }
            else
            {
                Console.WriteLine("No one in here!!!!");
            }
            Console.ReadLine();
        }
        public static double getDouble(string inputMsg, string errMsg,string exEr) 
        {
            double n;
            while (true)
            {
                try
                {
                    Console.Write(inputMsg);
                    n = double.Parse(Console.ReadLine());
                    if (n > 0)
                    {
                        return n;
                    }
                    else
                    {
                        Console.WriteLine(exEr);
                    }
                }
                catch (Exception e) 
                { 
                    Console.WriteLine(errMsg);
                }
                  
            }
        }
        public static int getInt(string inputMsg, string errMsg,string exEr)
        {
            int n;
            while (true) 
            {
                try
                {
                    Console.Write(inputMsg);
                    n = int.Parse(Console.ReadLine());
                    if (n > 0)
                    {
                        return n;
                    }
                    else
                    {
                        Console.WriteLine(exEr);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(errMsg);
                }
            }
        }

        public static int getAnInteger(String inputMsg, String errorMsg, int minValue, int maxValue)
        {
            int n;
            //swap
            if (minValue > maxValue)
            {
                int t = minValue;
                minValue = maxValue;
                maxValue = t;
            }

            while (true)
            {
                try
                {
                    Console.Write(inputMsg); 
                    n = int.Parse(Console.ReadLine());
                    if (n < minValue || n > maxValue)
                    {
                        throw new Exception();
                    }
                    else
                    {
                        return n;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(errorMsg); 
                }

            }
        }
    }
}

